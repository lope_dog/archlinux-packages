# Collection of PKGBUILDS for Achlinux:

- cantata-git

- clementine-qt5-git

- cmake-git

- cmake-release-git

- freebsd-manpages

- gcc-dev-git

- gcc-git

- gdb-git

- gdb-stable-git

- git-dev-git

- git-git

- git-main-git

- git-test-git

- gnupg-git

- gpgme-git

- kaffeine-git

- kdesvn-git

- kdesvn-stable-git

- kdevelop-dev-git

- kdevelop-next-git

- konversation-git

- konversation-stable-git

- krusader-git

- ktorrent-git

- ktorrent-stable-git

- libassuan-git

- libclc-git

- libdrm-git

- libgcrypt-git

- libgpg-error-git

- libinput-git

- libinput-stable-git

- libktorrent-git

- libktorrent-stable-git

- linux-api-headers-experimental-git

- linux-api-headers-trunk-git

- linux-firmware-git

- llvm-git

- llvm-svn

- lucjan-keyring

- lucjan-keyring-git

- mesa-dev-git

- mesa-rc-git

- modemmanager-git

- modemmanager-stable-git

- modemmanager-test-git

- netbsd-pkgsrc

- networkmanager-git

- networkmanager-stable-git

- networkmanager-test-git

- otter-browser-git

- pacman-contrib-git

- pacman-dev-git

- pacman-git

- pkgconf-git

- psi-clean-git

- psi-plus-clean-full-git

- psi-plus-clean-git

- psi-plus-l10n-git

- psi-plus-plugins-git

- psi-plus-resources-git

- psi-plus-webengine-full-git

- psi-plus-webengine-git

- psi-plus-webkit-full-git

- psi-plus-webkit-git

- psi-webengine-git

- psi-webkit-git

- qconf-git

- qtox-git

- sddm-dev-git

- sddm-git

- systemd-git

- systemd-stable-git

- toxcore-git

- utox-dev-git

- utox-git

- vault-git

- wayland-git

- wayland-protocols-git

- wayland-stable-git

- weston-git

- weston-stable-git

- xf86-video-intel-git

- xorg-server-git

- xorg-server-next-git

- yakuake-git

# Download:

```
git clone https://github.com/archlinux-lucjan/archlinux-packages.git

```
or

```
git clone https://gitlab.com/archlinux-lucjan/archlinux-packages.git

```

# Install:

```
cd /some_path/archlinux-packages/package_name
makepkg -srci

```
