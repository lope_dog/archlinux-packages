# Maintainer: Piotr Górski <lucjan.lucjanov@gmail.com>


pkgbase=llvm-svn
pkgname=('llvm-svn' 'llvm-libs-svn' 'llvm-ocaml-svn' 'clang-svn' 'clang-tools-extra-svn')
pkgdesc='Low Level Virtual Machine (svn version)'
pkgver=6.0.0svn.r317099
pkgrel=1
groups=('mesagit')
arch=('x86_64')
url="http://llvm.org"
license=('custom:University of Illinois')
makedepends=('cmake' 'libffi' 'python2' 'python2-sphinx' 'ocaml-findlib' 'ocaml-ctypes'
             'subversion')
options=('staticlibs' '!strip')
source=('llvm::svn+http://llvm.org/svn/llvm-project/llvm/trunk'
        'clang::svn+http://llvm.org/svn/llvm-project/cfe/trunk'
        'clang-tools-extra::svn+http://llvm.org/svn/llvm-project/clang-tools-extra/trunk'
        'compiler-rt::svn+http://llvm.org/svn/llvm-project/compiler-rt/trunk'
        llvm-Config-llvm-config.h llvm.patch)
md5sums=(SKIP SKIP SKIP SKIP SKIP SKIP)

pkgver() {
  cd llvm

  # This will almost match the output of `llvm-config --version` when the
    # LLVM_APPEND_VC_REV cmake flag is turned on. The only difference is
    # dash being replaced with underscore because of Pacman requirements.
    echo $(awk -F 'MAJOR |MINOR |PATCH |SUFFIX |)' \
            'BEGIN { ORS="." ; i=0 } \
             /set\(LLVM_VERSION_/ { print $2 ; i++ ; if (i==2) ORS="" } \
             END { print "\n" }' \
        CMakeLists.txt).r$(svnversion | tr -d [A-z])
}

prepare() {
  cd llvm
  
  # At the present, clang must reside inside the LLVM source code tree to build
  # See http://llvm.org/bugs/show_bug.cgi?id=4840
  rm -rf tools/clang projects/compiler-rt
  mv "$srcdir/clang" tools/clang
  mv "$srcdir/clang-tools-extra" tools/clang/tools/extra
  mv "$srcdir/compiler-rt" projects/compiler-rt
  
  #patch -Np1 -i ../llvm.patch

  mkdir build
}

build() {
  cd "$srcdir/llvm/build"
  
  # this will fix libclc-git that doesn't buit with clang-6.0 and -fno-plt (exposed through llvm-config)
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}

  #  -DLLVM_BUILD_DOCS=ON \
  cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_BUILD_LLVM_DYLIB=ON \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_FFI=ON \
    -DLLVM_BUILD_DOCS=OFF \
    -DLLVM_ENABLE_SPHINX=ON \
    -DLLVM_APPEND_VC_REV=OFF \
    -DSPHINX_OUTPUT_HTML:BOOL=ON \
    -DSPHINX_OUTPUT_MAN:BOOL=ON \
    -DSPHINX_WARNINGS_AS_ERRORS:BOOL=OFF \
    -DLLVM_ENABLE_DOXYGEN=OFF \
    -DFFI_INCLUDE_DIR=$(pkg-config --cflags-only-I libffi | cut -c3-) \
    -DLLVM_BINUTILS_INCDIR=/usr/include \
    ..

  make ocaml_doc
  make

  # Disable automatic installation of components that go into subpackages
  sed -i '/\(clang\|lldb\)\/cmake_install.cmake/d' tools/cmake_install.cmake
  sed -i '/extra\/cmake_install.cmake/d' tools/clang/tools/cmake_install.cmake
  sed -i '/compiler-rt\/cmake_install.cmake/d' projects/cmake_install.cmake
}

package_llvm-svn() {
  pkgdesc='Low Level Virtual Machine (svn version)'
  depends=("llvm-libs-svn=${pkgver}" 'perl')
  provides=('llvm')
  conflicts=('llvm')

  cd llvm

  make -C build DESTDIR="$pkgdir" install

  # Remove documentation sources
  rm -rf "$pkgdir"/usr/share/doc/llvm/html/{_sources,.buildinfo}

  # The runtime libraries go into llvm-libs, only keep versioned one
  mv -f "$pkgdir"/usr/lib/libLLVM-*.so "$srcdir"

  # Remove libs which conflict with llvm-libs
  rm -f "$pkgdir"/usr/lib/{libLLVM,libLTO,LLVMgold}.so
 
  # OCaml bindings go to a separate package
  rm -rf "$srcdir"/ocaml.{lib,doc}
  mv "$pkgdir/usr/lib/ocaml" "$srcdir/ocaml.lib"
  mv "$pkgdir/usr/share/doc/llvm/ocaml-html" "$srcdir/ocaml.doc"
  rm -r "$pkgdir/usr/share/doc"

  install -Dm644 LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_llvm-libs-svn() {
  pkgdesc='Low Level Virtual Machine library (svn version)'
  provides=('llvm-libs')
  depends=('gcc-libs' 'zlib' 'libffi' 'libedit' 'ncurses')

  install -d "$pkgdir/usr/lib"
  cp -P \
    "$srcdir"/libLLVM-*.so \
    "$pkgdir/usr/lib/"

  install -Dm644 "$srcdir"/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_llvm-ocaml-svn() {
  pkgdesc="OCaml bindings for LLVM (svn version)"
  depends=("llvm-svn=$pkgver-$pkgrel" 'ocaml' 'ocaml-ctypes')
  provides=('llvm-ocaml')
  conflicts=('llvm-ocaml')

  cd "$srcdir/llvm"

  install -d "$pkgdir"/{usr/lib,usr/share/doc}
  cp -a "$srcdir/ocaml.lib" "$pkgdir/usr/lib/ocaml"
  cp -a "$srcdir/ocaml.doc" "$pkgdir/usr/share/doc/$pkgname"

  install -Dm644 "$srcdir"/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_clang-svn() {
  pkgdesc="C language family frontend for LLVM (svn version)"
  url="http://clang.llvm.org/"
  depends=("llvm-svn=$pkgver-$pkgrel" 'gcc')
  optdepends=('llvm-libs: for compiling with -flto')
  provides=('clang' 'clang-analyzer')
  conflicts=('clang' 'clang-analyzer')
  replaces=('clang-analyzer-svn')
  
  cd "$srcdir/llvm"

  make -C build/tools/clang DESTDIR="$pkgdir" install
  make -C build/projects/compiler-rt DESTDIR="$pkgdir" install

  # Remove documentation sources
  rm -rf "$pkgdir"/usr/share/doc/$pkgname/html/{_sources,.buildinfo}

  # Install Python bindings
  install -d "$pkgdir/usr/lib/python2.7/site-packages"
  cp -a tools/clang/bindings/python/clang "$pkgdir/usr/lib/python2.7/site-packages/"
  python2 -m compileall "$pkgdir/usr/lib/python2.7/site-packages/clang"
  python2 -O -m compileall "$pkgdir/usr/lib/python2.7/site-packages/clang"

  # Use Python 2
  sed -i 's|/usr/bin/env python|&2|' \
    "$pkgdir/usr/bin/git-clang-format" \
    "$pkgdir/usr/bin/scan-view" \
    "$pkgdir/usr/share/clang/clang-format-diff.py" \
    "$pkgdir/usr/share/clang/clang-format-sublime.py" \
    "$pkgdir/usr/share/clang/clang-format.py"

  # move ccc-analyzer and c++-analyzer from libexec to bin
  mv -v "$pkgdir"/usr/libexec/* "$pkgdir"/usr/bin/
  sed -i 's|/libexec/|/bin/|' "${pkgdir}/usr/bin/scan-build"

  install -Dm644 "$srcdir"/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_clang-tools-extra-svn() {
  pkgdesc="Extra tools built using Clang's tooling APIs (svn version)"
  url="http://clang.llvm.org/"
  depends=("clang-svn=$pkgver-$pkgrel")
  provides=('clang-tools-extra')
  conflicts=('clang-tools-extra')

  cd "$srcdir/llvm"

  make -C build/tools/clang/tools/extra DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir"/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
