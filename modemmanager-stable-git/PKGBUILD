# Maintainer: Piotr Gorski <lucjan.lucjanov@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>

pkgbase=modemmanager-stable-git
pkgname=(modemmanager-stable-git libmm-glib-stable-git)
pkgver=1.4.20.21.gb3ba1474
pkgrel=1
pkgdesc="Mobile broadband modem management service"
arch=(x86_64)
url="http://www.freedesktop.org/wiki/Software/ModemManager/"
license=(GPL2 LGPL2.1)
depends=(systemd libgudev polkit ppp libqmi libmbim)
makedepends=(intltool gtk-doc gobject-introspection vala)
source=(git://anongit.freedesktop.org/ModemManager/ModemManager#branch=mm-1-4)
sha256sums=('SKIP')

prepare() {
  cd ModemManager
}

pkgver() {
  cd ModemManager

  # NOTE: The ModemManager maintainers seem to use "lightweight" tags
  #       rather than annotated tags, so we'll pass '--tags' to 'git'
  local ver="$(git describe --tags)"
  printf "%s" "${ver//-/.}"
}

build() {
  cd ModemManager
  ./autogen.sh
  ./configure --prefix=/usr \
        --sysconfdir=/etc \
        --localstatedir=/var \
        --sbindir=/usr/bin \
        --with-dbus-sys-dir=/usr/share/dbus-1/system.d \
        --with-udev-base-dir=/usr/lib/udev \
        --with-polkit=permissive \
        --with-suspend-resume=systemd \
        --enable-gtk-doc \
        --disable-static

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

#check() {
#  cd ModemManager-$pkgver
#  make -k check
#}

package_modemmanager-stable-git() {
  depends+=(libmm-glib)
  optdepends=('usb_modeswitch: install if your modem shows up as a storage drive')
  options=(!emptydirs)
  conflicts=('modemmanager')
  provides=('modemmanager')

  cd ModemManager
  make DESTDIR="$pkgdir" install
  make DESTDIR="$pkgdir" -C libmm-glib uninstall
  make DESTDIR="$pkgdir" -C vapi uninstall

  # Some stuff to move is left over
  mv "$pkgdir/usr/include" ..
  mv "$pkgdir/usr/lib/pkgconfig" ..
}

package_libmm-glib-stable-git() {
  pkgdesc="ModemManager library"
  depends=(glib2)
  conflicts=('libmm-glib')
  provides=('libmm-glib')

  install -d "$pkgdir/usr/lib"
  mv include "$pkgdir/usr"
  mv pkgconfig "$pkgdir/usr/lib"

  cd ModemManager
  make DESTDIR="$pkgdir" -C libmm-glib install
  make DESTDIR="$pkgdir" -C vapi install
}
